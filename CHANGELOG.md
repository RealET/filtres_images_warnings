# Changelog

## [Unreleased]

## Added

- Fichier `README.md`

## Changed

- Compatible SPIP 4.2.0-dev
